#Manual de usuario: **Galactic Road**

###Tabla de contenido

1. [Instalación](#insta)
2. [Como jugar](#jugar)
3. [Obstáculos](#obst)
4. [Mapas](#map)


----------
##<a id="insta"></a>Instalación
Requisitos: Python 3.x.x
###Para Linux
Instalar las librerías python3-pil python3-pil.imagetk. Abrir la consola e ir al directorio del juego:

	$ sh requirements.sh
Para correr el juego ir a la carpeta **core** y ejecutar _core.py_
	
	$ python3 core.py
Usar permisos de administrador si es necesario
###Para Windows 
Instalar las librerías python3-pil python3-pil.imagetk, ver el [link](https://pypi.python.org/pypi/Pillow/2.1.0). Para correr el juego ir a la carpeta **core** y ejecutar _core.py_

----------
##<a id="jugar"></a>Como jugar

El objetivo es llegar a la meta y contar con el suficiente plutonio, combustible para hacerlo. Elementalmente este circuito esta arreglado y perderás plutonio progresivamente así que habrá una que otra recarga en el camino.
![Plutonio](https://lh3.googleusercontent.com/z56K4-G3sIRUihnzmGL2Er23GgMaIltS7njLQk-Jb4d_JR0beos_AfaEPlc1fCszwsq5-MIQNOsXIIw=w1440-h721 "Plutonio")
Para hacer acelerar el vehículo usar la letra B. Para el movimiento lateral usar las lechas respectivamente; Izquierda con arrow left y derecha con arrow right. **Cuidado con el borde del camino o te estrellaras.**


----------
##<a id="obst"></a>Obstáculos
### Manchas de ácido
![Acido](https://lh4.googleusercontent.com/ESwNNLP9T9kh5-3MAwDG6qdScsJWQ8q2C4d6ajikiEq2f4MYWy0YZG91q2uqRgb1o4h71aFixpmKpgM=w1440-h721 "Acido")

Algunos corredores descuidados no hacen las revisiones correspondientes, trata de esquivarlas, sino perderás el control.
###Runner
![Runner](https://lh3.googleusercontent.com/T6ZIT-cUrY8p3murqIxBNpPxsRt2KfKabwR_JH2E5okfKHfUPk8VYkszFvSb5P8VGACwgIkeEbAvtHY=w1440-h721 "Runner")

Un humado al volante, es un poco lento pero no te confíes, su tamaño te puede hacer una mala jugada.
###Yordel
![Yordel](https://lh5.googleusercontent.com/MywTUEeC3nmVtOZXZmjNv4P7jODl9jtsdoGyJ3Ti7QSGuMwVWKPJnYQMzJwX6tqtGrE2cs0dJzqyL84=w1440-h721 "Yordel")

Alienígena del planeta Era, esta un poco loco. Cada vez que se siente amenazado cambia de carril inesperadamente, osea, siempre.
###Robot
![Robot](https://lh4.googleusercontent.com/JTpu2lh0BOYt2ga_kNPJfFb_2CdcmEnz1eE8znmhbkWSEJcnTM2OP0gPeNBBn0IXGTRkCYPuXSL3HKw=w1440-h721 "Robot")

Inteligencia artificial, una muestra del gran ingenio humano. No se sabe como llego al circuito pero es el mas peligroso, cuando te topes con el ten cuidado, te perseguirá.

----------
##<a id="map"></a>Mapas
![LVL 1](https://lh5.googleusercontent.com/wEMKhxqKPEr5IerVSYn8-nI7muPZ6D3thpatFvFAD2B15I3XxzBrdQEuKIZtk7vCdRK0aZ8FJCIIvUE=w1440-h721 "LVL 1")

![LVL 2](https://lh6.googleusercontent.com/QhYcT8JKq-f4ScDAuf4yf5BHauK0UW1rMrwfAG0mtQNdVUO1uawlkDkOuYIauOz2xXgGZ9Vd0sLXIaQ=w1440-h721 "LVL 2")

![LVL 3](https://lh4.googleusercontent.com/c_AZSn2jfMO-vQmFdACekgDYHLO-77klbAfQsaiK6TkhbZCMmOTINy1GyTXRO_r6AO4PL4DFfOJ02Io=w1440-h721 "LVL 3")

![LVL 4](https://lh5.googleusercontent.com/HbfC_nzIxvvtOGOrVUHf0LkxJxBHYm5FYZGIMq-0rc6VC4LjznleozE7TbHtkcnh8U_H-P8N8_EXFmc=w1440-h721 "LVL 4")
# Dimensiones
width_game = 1000
height_game = 600

# Funcion para cambiar de ventana
def change_layer(layer):
  layer.tkraise()

# Funcion para agregar imagenes
def add_image(path, dim = None):
  from PIL import Image, ImageTk

  img = Image.open(path)
  if dim != None:
    img = img.resize((dim[0], dim[1]), Image.ANTIALIAS)
  tk_bg = ImageTk.PhotoImage(img)

  return tk_bg

def user_validate(user_one):
  if user_one == "":
    return False
  else:
    return True

def back_game(layer, menu):
  layer.grid_forget()
  change_layer(menu)

############
#   Game   #
############

# Parametros
# data: Diccionario con atos del usuario. Revisar user_data.py
# canvas: Canvas del juego
# user: Objeto del jugador en el canvas

def explosion(data, canvas, user, cars):
  from time import sleep

  data['collapse'].set(True)
  canvas.itemconfig(user, state='hidden')
  boom_frame = add_image("../assets/img/boom_0.png")
  boom = canvas.create_image(canvas.coords(user)[0],
                                  canvas.coords(user)[1],image=boom_frame)
  out(canvas, cars)
  for x in range(1,7):
    boom_frame = add_image("../assets/img/boom_{}.png".format(x))
    canvas.itemconfig(boom, image=boom_frame)
    canvas.update()
    # Duerme el proceso por 0.1 segundos
    # Los necesarios para correr en 10 fps
    sleep(0.1)
  canvas.delete(boom)
  canvas.itemconfig(user, state='disabled')
  canvas.coords(user, 400, 500)
  data['collapse'].set(False)

coords_used = []

# Parametros
# value_x: Lista con rango para coordenada en eje x
# value_y: Lista con rango para coordenada en eje y
def get_pos(value_x, value_y):
  from random import randint

  x = randint(value_x[0], value_x[1])
  y = randint(value_y[0], value_y[1])
  for coord in coords_used:
    while abs(x - coord[0]) < 100 and abs(y - coord[1]) < 180:
      x = randint(value_x[0], value_x[1])
      y = randint(value_y[0], value_y[1])
  coords_used.append([x, y])
  return x, y

# Parametros
# canvas: El canvas del juego
# obstacle: Diccionario que contiene las caracterizticas del obstaculo
#   img: Image del obstaculo
#   dim: Lista con dimensiones del obstaculo
#   name: nombre del obstaculo
#   max: Maximo de veces que aparacera el obstaculo
#   same: Numero de objetos que puden aparecer en simultaneo
# dic_obstacle: lista donde se cargaran los obstaculos
def load_obstacles(canvas, obstacle, dic_obstacles):
  from random import randint

  if 'dim' in obstacle:
    img = add_image(obstacle['img'], obstacle['dim'])
  else:
    img = add_image(obstacle['img'])

  if obstacle['name'] == 'acid':
    canvas.img_acid = img
  elif obstacle['name'] == 'runner':
    canvas.img_runner = img
  elif obstacle['name'] == 'gas':
    canvas.img_gas = img
  elif obstacle['name'] == 'yordel':
    canvas.img_yordel = img
  elif obstacle['name'] == 'robot':
    canvas.img_robot = img
  dic_obstacles[obstacle['name']] = {'cant': 1, 'max': obstacle['max'], 'items': []}
  for a in range(obstacle['same']):
    if obstacle['name'] == 'gas':
      x, y = get_pos([250, 565], [-1200, -100])
    else:
      x, y = get_pos([250, 565], [-600, -100])
      if obstacle['name'] != 'acid':
        dic_obstacles[obstacle['name']]['add_vel'] = obstacle['add_vel']
        dic_obstacles[obstacle['name']]['move'] = True
    item = canvas.create_image(x, y, image=img, anchor='se')
    dic_obstacles[obstacle['name']]['items'].append(item)

# Parametros:
# coord: Lista de posiciones [x, y]
def delete_coords(coord):
  for old_coord in coords_used:
    while coord[0] == old_coord[0]:
      coords_used.remove(old_coord)
      return True

# Parametros
# canvas: Canvas del juego
# item: Objeto en el canvas
# obst: Diccionario con caracterizticas del obstaculo
#     cant: Numero de vecez que ha aparecido
#     max: Maximo que de veces que aparecera
#     items: Lista de objetos en el canvas
# type: Texto con nombre del obstaculo
def check_coords(canvas, item, obst, type):
  if type == 'acid':
    if canvas.coords(item)[1] > 2000:
      if obst['cant'] < obst['max']:
        delete_coords(canvas.coords(item))
        new_x, new_y = get_pos([250, 565], [-600, -100])
        canvas.coords(item, new_x, new_y)
        obst['cant'] += 1
  elif type == 'runner':
    if canvas.coords(item)[1] > 800:
      if obst['cant'] < obst['max']:
        delete_coords(canvas.coords(item))
        new_x, new_y = get_pos([250, 565], [-600, -100])
        canvas.coords(item, new_x, new_y)
        obst['cant'] += 1
  elif type == 'gas':
    if canvas.coords(item)[1] > 1200:
      if obst['cant'] < obst['max']:
        canvas.itemconfig(item, state='disabled')
        delete_coords(canvas.coords(item))
        new_x, new_y = get_pos([250, 565], [-1200, -100])
        canvas.coords(item, new_x, new_y)
        obst['cant'] += 1
  elif type == 'yordel':
    if canvas.coords(item)[1] > 1000:
      if obst['cant'] < obst['max']:
        delete_coords(canvas.coords(item))
        new_x, new_y = get_pos([250, 565], [-600, -100])
        canvas.coords(item, new_x, new_y)
        obst['move'] = True
        obst['cant'] += 1
  elif type == 'robot':
    if canvas.coords(item)[1] > 1500:
      if obst['cant'] < obst['max']:
        delete_coords(canvas.coords(item))
        new_x, new_y = get_pos([250, 565], [-600, -100])
        canvas.coords(item, new_x, new_y)
        obst['move'] = True
        obst['cant'] += 1

# Parametros
# canvas: Canvas del juego
# car: Diccionario con obstaculo
# user: Objeto del jugador en el mapa
# robot: Boleano que revisa si es el carro inteligente
def move_obstacles(canvas, car, user, robot):
  from random import choice

  if car['move']:
    if canvas.coords(car['items'][0])[1] > 50:
      if robot:
        distance = canvas.coords(user)[0] - canvas.coords(car['items'][0])[0]
        # Pseudo posicionamiento nuevo
        for coord in coords_used:
          while canvas.coords(user)[0] + distance in coord:
            if distance >= 0:
              distance -= 50
            else:
              distance += 50
        canvas.move(car['items'][0], distance, 0)
      else:
        distance = choice([-50, 50])
        x = canvas.coords(car['items'][0])[0]
        # Pseudo posicionamiento nuevo
        while distance + x > 565 or distance + x < 235:
          distance = choice([-50, 50])
        # for coord in coords_used:
        #   while canvas.coords(car['items'][0])[0] + distance in coord:
        #     distance = choice([-50, 50])
        canvas.move(car['items'][0], distance, 0)
      car['move'] = False
  else:
    return False

def out(canvas, cars):
  from random import randint

  for car in cars:
    if canvas.coords(car)[1] > 0:
      y = randint(-600, -100)
      distance = -1*abs(canvas.coords(car)[1] - y)
      canvas.move(car, 0, distance)
# Parametros
# data: Revisar user_one user_data.py
# canvas: Canvas del juego
# user: Objeto del jugador en el canvas
# car: Opcional. Boleano. Merma combustuble
def slow_move(data, canvas, user, cars, car = None):
  from random import choice
  from time import time

  data['less'].set(True)
  data['disorient'].set(True)
  canvas.move(user, choice([-50, 50]), 0)
  if canvas.coords(user)[0] < 235 or canvas.coords(user)[0] > 565:
    explosion(data, canvas, user, cars)
    data['less'].set(False)
    data['disorient'].set(False)
    return False
  data['disorient'].set(False)
  if car:
    data['gas'].set(data['gas'].get() - 10)
  data['time_less'].set(time())

# Parametros
# canvas: Canvas del juego
# mapa: Objeto del mapa en el canvas
# player: Objeto del jugador
# vel: Numero de pixeles en y que se movera
# obstacles: Diccionario con los obstaculos cargados
#  name: Nombre del obstaculo
#     cant: Numero de vecez que ha aparecido
#     max: Maximo que de veces que aparecera
#     items: Lista con objetos en el canvas
def move(canvas, mapa, vel, obstacles, user):
  canvas.move(mapa, 0, vel)
  # Acido
  canvas.move(obstacles['acid']['items'][0], 0, vel)
  check_coords(canvas, obstacles['acid']['items'][0], obstacles['acid'], 'acid')
  # Runner
  for item in obstacles['runner']['items']:
    canvas.move(item, 0, vel)
    check_coords(canvas, item, obstacles['runner'], 'runner')
  # Yordel
  canvas.move(obstacles['yordel']['items'][0], 0, vel+obstacles['yordel']['add_vel'])
  move_obstacles(canvas, obstacles['yordel'], user, False)
  check_coords(canvas, obstacles['yordel']['items'][0], obstacles['yordel'], 'yordel')
  # Robot
  canvas.move(obstacles['robot']['items'][0], 0, vel+obstacles['robot']['add_vel'])
  move_obstacles(canvas, obstacles['robot'], user, True)
  check_coords(canvas, obstacles['robot']['items'][0], obstacles['robot'], 'robot')
  # Plutonium
  canvas.move(obstacles['gas']['items'][0], 0, vel)
  check_coords(canvas, obstacles['gas']['items'][0], obstacles['gas'], 'gas')

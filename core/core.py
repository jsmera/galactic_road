from tkinter import *
from tkinter import ttk
from time import time
from mixins import change_layer, width_game, height_game, add_image, user_validate



# Configuracion basica
root = Tk()
root.title("Galatic Road")
# Ajuste de ventana
root.geometry('{}x{}'.format(width_game, height_game))
root.resizable(width=False, height=False)

# Datos del usuario
from user_data import user_one

############
#  Layers  #
############

# La variable state determina si el frame ya fue creado

layer_menu = Frame(root, width=width_game, height=height_game)
state_menu = False
layer_name = Frame(root, width=width_game, height=height_game)
state_name = False
layer_lvl = Frame(root, width=width_game, height=height_game)
state_lvl = False
layer_game = Frame(root, width=width_game, height=height_game)

############
#   Menu   #
############

def Menu(menu, next):
  global state_menu
  # Marco del menu principal
  menu.grid(row=0, column=0, sticky='news')

  # Imagen de fondo
  tk_bg = add_image('../assets/img/bg.gif', [width_game, height_game])
  canvas_menu = Canvas(menu, width=width_game, height=height_game, bg='black')
  canvas_menu.pack()
  # Referencia: http://stackoverflow.com/questions/26479728/tkinter-canvas-image-not-displaying
  canvas_menu.tk_bg = tk_bg
  canvas_menu.create_image(0, 0, image=tk_bg, anchor=NW)

  # Titulo
  canvas_menu.create_text(250, 84, fill='white', anchor=NW, font='Inconsolata 60 bold',
               text='GALATIC ROAD')

  # Botones
  play_one = Button(menu, text='ONE PLAYER', bg='#000',
                    fg='white', font='Inconsolata 22 bold', width=12, bd=0,
                    command=lambda:Names(next, layer_lvl))
  # play_second = Button(menu, text='TWO PLAYER', bg='#000',
  #                     fg='white', font='Inconsolata 22 bold', width=12, bd=0)

  play_one.place(relx=0.5, rely=0.4, anchor=CENTER)
  #play_second.place(relx=0.5, rely=0.6, anchor=CENTER)

  # Primer marco que se activara
  state_menu = True
  change_layer(menu)

############
#  Names   #
############
def Names(layer, next):
  global state_name

  if state_name:
    change_layer(layer)
  else:
    # Variable que guarda nombre
    global user_one

    change_layer(layer)
    layer.grid(row=0, column=0, sticky='news')

    tk_bg = add_image('../assets/img/bg.gif', [width_game, height_game])

    canvas_names = Canvas(layer, width=width_game, height=height_game, bg='black')
    canvas_names.pack()
    canvas_names.tk_bg = tk_bg
    canvas_names.create_image(0, 0, image=tk_bg, anchor=NW)

    # Titulo
    canvas_names.create_text(100, 84, fill='white', anchor=NW, font='Inconsolata 60 bold',
                 text='WHAT IS YOUR NAME?')

    # Campo de usuario
    text_user = Entry(layer, textvariable=user_one['name'], font='Inconsolata 22 bold',
                      width=15, bd=2)
    # Botones
    next_layer = Button(layer, text='Next', bg='#000',
                        fg='white', font='Inconsolata 22 bold', width=12,
                        bd=0, command=lambda:continue_layer(user_one['name']))

    back_layer = Button(layer, text='Back', bg='#000', fg='white',
                        font='Inconsolata 22 bold', width=12,
                        bd=0, command=lambda:change_layer(layer_menu))

    text_user.place(relx=0.5, rely=0.4, anchor=CENTER)
    back_layer.place(relx=0.3, rely=0.6, anchor=CENTER)
    next_layer.place(relx=0.7, rely=0.6, anchor=CENTER)
    state_name = True

    # Funcion que detecta si el campo del nombre esta vacio
    # Recibe el objeto StringVar()
    def continue_layer(user):
      if user_validate(user.get()):
        Choices(layer_lvl, layer_game, user_one)
      else:
        return False


def Choices(layer, next, data):
  global state_lvl

  if state_lvl:
    change_layer(layer)
  else:
    change_layer(layer)
    layer.grid(row=0, column=0, sticky='news')

    tk_bg = add_image('../assets/img/bg.gif', [width_game, height_game])

    canvas_lvl = Canvas(layer, width=width_game, height=height_game, bg='black')
    canvas_lvl.pack()
    canvas_lvl.tk_bg = tk_bg
    canvas_lvl.create_image(0, 0, image=tk_bg, anchor=NW)

    # Titulo
    canvas_lvl.create_text(250, 84, fill='white', anchor=NW, font='Inconsolata 60 bold',
                 text='CHOOSE A LVL')

    # Niveles

    lvl_1 = Button(layer, text="LVL 1", bg='#000', fg='white',
                        font='Inconsolata 22 bold',
                        bd=0, command=lambda:Game(layer_game, 'lvl_1', data))
    lvl_2 = Button(layer, text="LVL 2", bg='#000', fg='white',
                        font='Inconsolata 22 bold',
                        bd=0, command=lambda:Game(layer_game, 'lvl_2', data))
    lvl_3 = Button(layer, text="LVL 3", bg='#000', fg='white',
                        font='Inconsolata 22 bold',
                        bd=0, command=lambda:Game(layer_game, 'lvl_3', data))
    lvl_4 = Button(layer, text="LVL 4", bg='#000', fg='white',
                        font='Inconsolata 22 bold',
                        bd=0, command=lambda:Game(layer_game, 'lvl_4', data))
    lvl_5 = Button(layer, text="LVL 5", bg='#000', fg='white',
                        font='Inconsolata 22 bold',
                        bd=0, command=lambda:Game(layer_game, 'lvl_5', data))

    # Botones
    back_layer = Button(layer, text='Back', bg='#000', fg='white',
                        font='Inconsolata 22 bold', width=12,
                        bd=0, command=lambda:change_layer(layer_name))

    lvl_1.place(relx=0.1, rely=0.5)
    lvl_2.place(relx=0.25, rely=0.5)
    lvl_3.place(relx=0.4, rely=0.5)
    lvl_4.place(relx=0.55, rely=0.5)
    lvl_5.place(relx=0.7, rely=0.5)
    back_layer.place(relx=0.5, rely=0.7, anchor=CENTER)
    state_lvl = True

# data: Diccionario
def Game(layer, lvl, data):
  # Funciones del juego
  from mixins import explosion, load_obstacles, move, slow_move, out, back_game
  # Parametros
  # layer: Frame del juego
  # lvl: Texto que contiene el nivel con estilo lvl_n
  # data: Revisar user_one en user_data.py
  def again_game(layer, lvl, data):
    layer.grid_forget()
    Game(layer, lvl, data)
  # Constantes
  # Va por nivel:
  #   obstacles: Lista con obstaculos
  #   vel_max: Maximo de velocidad alcanzada, real.
  #   limit_y: Limite de la meta en y del mapa
  #   limit_x: Lista con limites en y del mapa
  #   cant_gas: Cantidad de gasolina que se puede tener por juego
  #   real_vel': Maximo de velocidad alcanzada, simulada.
  MAXIMA = {
    'lvl_1': {
      'obstacles': [
        {'name': 'acid', 'max': 15, 'img': '../assets/img/acid.png', 'dim': [40, 41], 'same': 1},
        {'name': 'runner', 'max': 60, 'img': '../assets/img/runner.png', 'dim': [50, 101],
          'same': 2, 'add_vel': 0},
        {'name': 'gas', 'max': 30, 'img': '../assets/img/gas.png', 'dim': [60, 40], 'same': 1},
        {'name': 'yordel', 'max': 30, 'img': '../assets/img/yordel.png', 'dim': [50, 101],
          'same': 1, 'add_vel': 0.5},
        {'name': 'robot', 'max': 25, 'img': '../assets/img/robot.png', 'dim': [40, 91],
          'same': 1, 'add_vel': 1}
      ],
      'vel_max': 6,
      'limit_y': 28800,
      'limit_x': [235, 565],
      'cant_gas': 2000,
      'real_vel': 180
    },
    'lvl_2': {
      'obstacles': [
        {'name': 'acid', 'max': 10, 'img': '../assets/img/acid.png', 'dim': [40, 41], 'same': 1},
        {'name': 'runner', 'max': 28, 'img': '../assets/img/runner.png', 'dim': [50, 101],
          'same': 2, 'add_vel': 0},
        {'name': 'gas', 'max': 10, 'img': '../assets/img/gas.png', 'dim': [60, 40], 'same': 1},
        {'name': 'yordel', 'max': 20, 'img': '../assets/img/yordel.png', 'dim': [50, 101],
          'same': 1, 'add_vel': 1},
        {'name': 'robot', 'max': 20, 'img': '../assets/img/robot.png', 'dim': [40, 91],
          'same': 1, 'add_vel': 1.5}
      ],
      'vel_max': 9,
      'limit_y': 17600,
      'limit_x': [235, 565],
      'cant_gas': 700,
      'real_vel': 270
    },
    'lvl_3': {
      'obstacles': [
        {'name': 'acid', 'max': 10, 'img': '../assets/img/acid.png', 'dim': [40, 41], 'same': 1},
        {'name': 'runner', 'max': 28, 'img': '../assets/img/runner.png', 'dim': [50, 101],
          'same': 2, 'add_vel': 0},
        {'name': 'gas', 'max': 5, 'img': '../assets/img/gas.png', 'dim': [60, 40], 'same': 1},
        {'name': 'yordel', 'max': 20, 'img': '../assets/img/yordel.png', 'dim': [50, 101],
          'same': 1, 'add_vel': 1.5},
        {'name': 'robot', 'max': 20, 'img': '../assets/img/robot.png', 'dim': [40, 91],
          'same': 1, 'add_vel': 1.8}
      ],
      'vel_max': 9,
      'limit_y': 17600,
      'limit_x': [235, 565],
      'cant_gas': 700,
      'real_vel': 360
    },
    'lvl_4': {
      'obstacles': [
        {'name': 'acid', 'max': 10, 'img': '../assets/img/acid.png', 'dim': [40, 41], 'same': 1},
        {'name': 'runner', 'max': 15, 'img': '../assets/img/runner.png', 'dim': [50, 101],
          'same': 2, 'add_vel': 0},
        {'name': 'gas', 'max': 5, 'img': '../assets/img/gas.png', 'dim': [60, 40], 'same': 1},
        {'name': 'yordel', 'max': 20, 'img': '../assets/img/yordel.png', 'dim': [50, 101],
          'same': 1, 'add_vel': 1.5},
        {'name': 'robot', 'max': 20, 'img': '../assets/img/robot.png', 'dim': [40, 91],
          'same': 1, 'add_vel': 1.8}
      ],
      'vel_max': 12,
      'limit_y': 17600,
      'limit_x': [235, 565],
      'cant_gas': 900,
      'real_vel': 450
    },
    'lvl_5': {
      'obstacles': [
        {'name': 'acid', 'max': 15, 'img': '../assets/img/acid.png', 'dim': [40, 41], 'same': 1},
        {'name': 'runner', 'max': 28, 'img': '../assets/img/runner.png', 'dim': [50, 101],
          'same': 2, 'add_vel': 0},
        {'name': 'gas', 'max': 5, 'img': '../assets/img/gas.png', 'dim': [60, 40], 'same': 1},
        {'name': 'yordel', 'max': 20, 'img': '../assets/img/yordel.png', 'dim': [50, 101],
          'same': 1, 'add_vel': 1.5},
        {'name': 'robot', 'max': 20, 'img': '../assets/img/robot.png', 'dim': [40, 91],
          'same': 1, 'add_vel': 1.8}
      ],
      'vel_max': 12,
      'limit_y': 17600,
      'limit_x': [235, 565],
      'cant_gas': 900,
      'real_vel': 450
    }
  }

  layer.grid(row=0, column=0, sticky='news')
  change_layer(layer)
  # Zona de juego
  canvas_game = Canvas(layer, width=800, height=height_game, bg='black')
  canvas_game.grid(row=0, column=0)
  # Panel de usuario
  data_game = Frame(layer, width=200, height=height_game, bg='black')
  data_game.grid(row=0, column=1)

  label_name = Label(data_game, text='Name', fg='white',
                    font='Inconsolata 20 bold', bg='black')
  name_user = Label(data_game, textvariable=data['name'], fg='white',
                    font='Inconsolata 20 bold', bg='black')
  label_speed = Label(data_game, text='Speed', fg='white',
                    font='Inconsolata 20 bold', bg='black')
  speed_user = Label(data_game, textvariable=data['speed'], fg='white',
                    font='Inconsolata 20 bold', bg='black')
  label_gas = Label(data_game, text='Plutonium', fg='white',
                    font='Inconsolata 20 bold', bg='black')
  gas_user = Label(data_game, textvariable=data['gas'], fg='white',
                  font='Inconsolata 20 bold', bg='black')

  label_name.place(relx=0.1, rely=0.1)
  name_user.place(relx=0.15, rely=0.15)
  label_speed.place(relx=0.1, rely=0.3)
  speed_user.place(relx=0.15, rely=0.35)
  label_gas.place(relx=0.1, rely=0.5)
  gas_user.place(relx=0.15, rely=0.55)

  # Parametros:
  # lvl: Texto que contiene el nivel con estilo lvl_n
  # Carga el juego segun el nivel y los datos que tenga en MAXIMA
  def load_game(lvl):
    # Diccionario que guarda los obstaculos en el mapa
    # de la siguiente forma:
    # {'name': {max: obstacle['max'], 'cant': 1, 'items': [], 'add_vel': 2}}
    # Donde name es el tipo de obstaculo, cant es las vecez que ha aparecido
    # max es el maximo que de veces que aparecera
    # items es una lista con objetos en el canvas
    # add_vel es el aumento de velocidad segun corredor
    obstacles = {}
    data['gas'].set(MAXIMA[lvl]['cant_gas'])

    # Cargando imagenes #
    # Fondo
    lvl_bg = add_image("../assets/img/lvl/{}.png".format(lvl))
    # Jugador
    img_player = add_image("../assets/img/player_one.png", [50, 101])

    canvas_game.lvl_bg = lvl_bg
    map_bg = canvas_game.create_image(800, 600, image=lvl_bg, anchor=SE)
    #############
    # Obtaculos #
    #############
    # Manchas de acido
    load_obstacles(canvas_game, MAXIMA[lvl]['obstacles'][0], obstacles)
    # Runner
    load_obstacles(canvas_game, MAXIMA[lvl]['obstacles'][1], obstacles)
    # Yordel
    load_obstacles(canvas_game, MAXIMA[lvl]['obstacles'][3], obstacles)
    # Robot
    load_obstacles(canvas_game, MAXIMA[lvl]['obstacles'][4], obstacles)
    # Plutonium
    load_obstacles(canvas_game, MAXIMA[lvl]['obstacles'][2], obstacles)
    # Jugador
    canvas_game.img_player = img_player
    player = canvas_game.create_image(400, 500, image=img_player, anchor=CENTER)

    keys_game = []
    # 56: B
    # 114: Right
    # 113: Left
    # Limetes carretera en x (235, 565)

    # Se crea una lista que guarda los keycode de las teclas oprimidas
    def keydown(event):
      if not event.keycode in keys_game:
        keys_game.append(event.keycode)
    
    # Elimina las teclas de la lista despues de soltarla
    def keyup(event):
      if event.keycode in keys_game:
        keys_game.pop(keys_game.index(event.keycode))

    # Funcion que detecta las letras precionadas
    def key():
      # Meta
      if canvas_game.coords(map_bg)[1] < MAXIMA[lvl]['limit_y'] and data['gas'].get() > 0:
        if not data['collapse'].get():
          ##############
          # Colisiones #
          ##############
          cars = obstacles['runner']['items']+obstacles['yordel']['items']+obstacles['robot']['items']
          x1, y1 = canvas_game.coords(player)
          under_player = canvas_game.find_overlapping(x1, y1, x1 + 50, y1 + 171)
          # Para acido
          if obstacles['acid']['items'][0] in under_player:
            slow_move(data, canvas_game, player, cars)
          # Para carros
          for item in cars:
            if item in under_player:
              slow_move(data, canvas_game, player, cars, car=True)
          # Para platino
          if obstacles['gas']['items'][0] in under_player:
            data['gas'].set(data['gas'].get() + 200)
            canvas_game.itemconfig(obstacles['gas']['items'][0], state='hidden')
          ##################
          # Fin colisiones #
          ##################
          if 56 in keys_game:
            def fluid(vel):
              # Diminucion de velocidad si se colisiona
              if data['less'].get():
                vel_max = MAXIMA[lvl]['vel_max'] - 3
                if time() - data['time_less'].get() > 1:
                  data['less'].set(False)
              else:
                vel_max = MAXIMA[lvl]['vel_max']
              # Velocidad maxima alcanzada en 0.9 seg aprox
              if data['collapse'].get():
                # Si se colisiona permanece estatico
                return False
              else:
                if vel >= vel_max:
                  move(canvas_game, map_bg, vel, obstacles, player)
                  data['speed'].set(data['speed'].get() + MAXIMA[lvl]['real_vel']//vel)
                else:
                  move(canvas_game, map_bg, vel, obstacles, player)
                  if vel != 0:
                    data['speed'].set(data['speed'].get() + MAXIMA[lvl]['real_vel']//vel)
                  else:
                    data['speed'].set(50)
                  root.after(300, lambda:fluid(vel+3))
            fluid(0)
          else:
            data['speed'].set(0)
            # cars = obstacles['runner']['items']+obstacles['yordel']['items']+obstacles['robot']['items']
            # Saca los carros de la pantalla si se detiene
            out(canvas_game, cars)
          if 114 in keys_game and not data['disorient'].get():
            if canvas_game.coords(player)[0] < MAXIMA[lvl]['limit_x'][1]:
              # Aumento en aceleracion
              if 56 in keys_game:
                canvas_game.move(player, 5, 0)
              else:
                canvas_game.move(player, 3, 0)
            else:
              explosion(data, canvas_game, player, cars)
          if 113 in keys_game and not data['disorient'].get():
            if canvas_game.coords(player)[0] > MAXIMA[lvl]['limit_x'][0]:
              # Aumento en aceleracion
              if 56 in keys_game:
                canvas_game.move(player, -5, 0)
              else:
                canvas_game.move(player, -3, 0)
            else:
              explosion(data, canvas_game, player, cars)
        data['gas'].set(data['gas'].get() - 1)
      else:
        if data['gas'].get() > 0:
          w = Message(layer, text="Win", width=80, fg='white',
                      font='Inconsolata 20 bold', bg='black')
        else:
          w = Message(layer, text="Lose", width=80, fg='white',
                      font='Inconsolata 20 bold', bg='black')
        data['speed'].set(0)
        data['collapse'].set(False)
        data['disorient'].set(False)
        data['less'].set(False)
        back = Button(layer, text="Back", fg='white',
                     font='Inconsolata 20 bold', bg='black',
                     command=lambda:back_game(layer, layer_lvl))
        again = Button(layer, text="Again", fg='white',
                      font='Inconsolata 20 bold', bg='black',
                      command=lambda:again_game(layer, lvl, data))
        w.place(relx=0.35, rely=0.3)
        again.place(relx=0.3, rely=0.4)
        back.place(relx=0.4, rely=0.4)
        return False
      root.after(10, key)

    layer.bind("<KeyPress>", keydown)
    layer.bind("<KeyRelease>", keyup)
    layer.focus_set()
    key()

  load_game(lvl)

Menu(layer_menu, layer_name)
root.mainloop()

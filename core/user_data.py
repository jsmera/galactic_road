from tkinter import StringVar, BooleanVar, DoubleVar, IntVar



# Datos de usuarios
# name: String. Nombre del usuario
# collapse: Boleano. Si se colisiono con el borde de la carretera
# disorient: Boleano. Si necesita cambiar de carril
# less: Boleano. Si colisiono con algun carro y merma su velocidad
# time_less: Floar. Tiempo en que se colisiono con algun obstaculo
# speed: Int. Velocidad simulada
# gas: Int. Cantidad de combustible
user_one = {
  'name': StringVar(),
  'collapse': BooleanVar(),
  'disorient': BooleanVar(),
  'less': BooleanVar(),
  'time_less': DoubleVar(),
  'speed': IntVar(),
  'gas': IntVar()
}
